**External files for grmsem package**

For example analyses in the vignette, two different data sets were modelled, with parameters detailed in the vignette:

1. For quick demonstration purposes, a small tri-variate toy data set was simulated, assuming an underlying Cholesky model with 100 observations per trait and low polygenicity (150 SNPs per genetic factor):

    a. Files for GRMSEM analyses

        * GRM matrix: G.small.RData
        * Tri-variate phenotype file: ph.small.RData
		
	b. Files for GREML analyses (GCTA)

        * Compressed GRM matrix: small.gcta.grm.gz
		* Binary GRM matrix: small.gcta.grm.bin
        * Quad-variate phenotype file: small.gcta.phe
        * ID file: small.gcta.grm.id
        
2. A large quad-variate data set was simulated assuming an underlying Cholesky model, with 5000 observations per trait and high polygenicity (5,000 SNPs per genetic factor). The corresponding files are stored in this directory:

    a. Files for GRMSEM analyses

        * GRM matrix: G.large.RData
        * Quad-variate phenotype file: ph.large.RData
        * Prefitted output file (Cholesky model): fit.large.RData
        
    b. Files for GREML analyses (GCTA)

        * Compressed GRM matrix: large.gcta.grm.gz
		* Binary GRM matrix: large.gcta.grm.bin
        * Quad-variate phenotype file: large.gcta.phe
        * ID file: large.gcta.grm.id  



